<?php

namespace ADW\DefconBundle\Security\EntryPoint;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Router;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface;

/**
 * Class ByIpEntryPoint
 *
 * @package AppBundle\Security
 * @author Artur Vesker
 */
class ByIpEntryPoint implements AuthenticationEntryPointInterface
{

    /**
     * @var string
     */
    protected $loginRoute;

    /**
     * @var Router
     */
    protected $router;

    /**
     * ByIpEntryPoint constructor.
     *
     * @param string $loginRoute
     * @param Router $router
     */
    public function __construct($loginRoute, Router $router)
    {
        $this->loginRoute = $loginRoute;
        $this->router = $router;
    }

    /**
     * @inheritdoc
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        if ($request->attributes->get('_route') === 'sonata_user_admin_security_login') {
            throw new AccessDeniedHttpException();
        }

        return new RedirectResponse($this->router->generate($this->loginRoute));
    }

}