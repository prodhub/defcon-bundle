<?php

namespace ADW\DefconBundle\EventListener;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\AnonymousToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Http\HttpUtils;

/**
 * Class UserIdleLogoutRequestListener
 *
 * @package ADW\DefconBundle\EventListener
 * @author Artur Vesker
 */
class UserIdleLogoutRequestListener
{

    /**
     * @var HttpUtils
     */
    protected $httpUtils;

    /**
     * @var int
     */
    protected $maxIdleTime;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;

    /**
     * UserIdleLogoutRequestListener constructor.
     *
     * @param TokenStorageInterface $tokenStorage
     * @param HttpUtils $httpUtils
     * @param int $maxIdleTime
     * @param $path
     */
    public function __construct(TokenStorageInterface $tokenStorage, HttpUtils $httpUtils, $maxIdleTime, $path)
    {
        $this->tokenStorage = $tokenStorage;
        $this->httpUtils = $httpUtils;
        $this->maxIdleTime = $maxIdleTime;
        $this->path = $path;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function onRequest(GetResponseEvent $event)
    {
        if (!$token = $this->tokenStorage->getToken()) {
            return;
        }

        if ($token instanceof AnonymousToken) {
            return;
        }

        if (!$token->hasAttribute('_last_used')) {
            $token->setAttribute('_last_used', time());
            return;
        }

        $lastUsed = $token->getAttribute('_last_used');

        $now = time();

        if (($now - $lastUsed) > $this->maxIdleTime) {
            $event->setResponse($this->httpUtils->createRedirectResponse($event->getRequest(), $this->path));
            $event->stopPropagation();
        }

        $token->setAttribute('_last_used', $now);
    }



}