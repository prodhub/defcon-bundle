<?php

namespace ADW\DefconBundle\EventListener;

use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

/**
 * Class ForceSecureCookiesResponseListener
 *
 * @package ADW\DefconBundle\EventListener
 * @author Artur Vesker
 */
class ForceSecureCookiesResponseListener
{

    /**
     * @param FilterResponseEvent $event
     */
    public function forceSecureCookies(FilterResponseEvent $event)
    {
        $headers = $event->getResponse()->headers;

        /**
         * @var Cookie $cookie
         */
        foreach ($headers->getCookies() as $cookie) {
            if ($cookie->isSecure()) {
                continue;
            }

            $headers->setCookie(new Cookie(
                $cookie->getName(),
                $cookie->getValue(),
                $cookie->getExpiresTime(),
                $cookie->getPath(),
                $cookie->getDomain(),
                true,
                $cookie->isHttpOnly()
            ));
        }
    }

}