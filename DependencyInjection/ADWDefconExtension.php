<?php

namespace ADW\DefconBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration.
 *
 * @link http://symfony.com/doc/current/cookbook/bundles/extension.html
 */
class ADWDefconExtension extends Extension
{
    /**
     * {@inheritdoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        if ($config['cookie']['force_secure']) {
            $forceSecureCookiesResponseListenerDefinition = new Definition('ADW\DefconBundle\EventListener\ForceSecureCookiesResponseListener');

            $forceSecureCookiesResponseListenerDefinition->addTag('kernel.event_listener', [
                'event' => 'kernel.response',
                'method' => 'forceSecureCookies',
                'priority' => -99
            ]);

            $container->setDefinition('defcon.cookie_force_secure', $forceSecureCookiesResponseListenerDefinition);
        }

        if ($config['user_idle_logout']['enabled']) {
            $userIdleLogoutRequestListener = new Definition('ADW\DefconBundle\EventListener\UserIdleLogoutRequestListener', [
                new Reference('security.token_storage'),
                new Reference('security.http_utils'),
                $config['user_idle_logout']['max_time'],
                $config['user_idle_logout']['path']
            ]);

            $userIdleLogoutRequestListener->addTag('kernel.event_listener', [
                'event' => 'kernel.request',
                'method' => 'onRequest'
            ]);

            $container->setDefinition('defcon.user_idle_logout', $userIdleLogoutRequestListener);
        }

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');
    }
}
